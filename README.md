Pin Manager Project

Guidelines to use services-

1. Enter a valid MSISDN in MSISDNEntity table by.
 a) Login into http://localhost:3306/pinmanager?useSSL=false
   i) user: root
   ii) password: root
 b) check MSISDNENTITY table as all the required tables will be created automatically and hence,
    insert into MSISDNENTITY values(34999112233);
 
2. Open a REST client like Postman
 a) For Generatation of PIN-
  1. POST APU: http://localhost:8085/pin-manager/generate
  2. Fill content-type as application/json in headers
  3. POST Request Body-
    {
       "msisdn" : "34999112233"
    }
  4. Send request.
  5. Response body:
  (i) If pin validated successfully 
   {
    "message": "PIN generated successfully",
    "data": {
        "msisdn": "34999112233",
        "pin": "9102"
        }
    }
 (ii) One can generate upto 3 pins as max attempt
  {
    "message": "You already attempted 3 times",
    "data": {
        "msisdn": "34999112233",
        "pin": null
    }
  }
 (iii) InValid pin response
  {
    "message": "Please enter a valid MSISDN which must be of 11 digit and registered with us.",
    "data": {
        "msisdn": "34999112233",
        "pin": null
    }
 }
  
  b) Pin Validation Rest API-
    1. PUT Request Body: http://localhost:8085/pin-manager/validate/
	2. Fill content-type as application/json in headers
	3. PUT Request Body-
	{
      "msisdn":"34999112233",
      "pin":"0382"
	}
	4. Send request
	5. Response body:
	(i) Success Pin Validation Response
	 {
       "message": "Pin validated successfully",
       "data": {
          "msisdn": "34999112233",
          "pin": "8589"
            }
        }
    (ii) Once a pin will be validated, and retry again then response is:
	{
    "message": "Pin validation failed",
    "data": {
        "msisdn": "34999112233",
        "pin": "8589"
        }
    }
    
   7. Pine wipe up service is running at every 5 mins to perform the operation of clearing out non-validated pins after 1 hr of their creation
	
