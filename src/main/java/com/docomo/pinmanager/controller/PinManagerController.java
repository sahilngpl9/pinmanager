package com.docomo.pinmanager.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.docomo.pinmanager.exception.PinManagerException;
import com.docomo.pinmanager.model.PinDto;
import com.docomo.pinmanager.model.PinResponse;
import com.docomo.pinmanager.service.PinService;

@RestController
@RequestMapping("/pin-manager")
public class PinManagerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PinManagerController.class);

	@Autowired
	private PinService pinService;
	
	@PostMapping("/generate")
	public ResponseEntity<PinResponse> generatePin(@RequestBody PinDto pinDto) {
		ResponseEntity<PinResponse> response= null;
		try {
			pinDto = pinService.generatePin(pinDto);
			response = new ResponseEntity<>(new PinResponse("PIN generated successfully", pinDto), HttpStatus.CREATED);
		} catch (PinManagerException e) {
			response = new ResponseEntity<>(new PinResponse(e.getErrorMessage(), pinDto), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			response = new ResponseEntity<>(new PinResponse("UnExpected Error, Please contact admin", pinDto), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PutMapping("/validate")
	public ResponseEntity<PinResponse> validatePin(@RequestBody PinDto pinDto) {
		ResponseEntity<PinResponse> response= null;
		try {
			if(pinService.validatePin(pinDto)) {
				response = new ResponseEntity<>(new PinResponse("Pin validated successfully", pinDto), HttpStatus.CREATED);
			}else {
				response = new ResponseEntity<>(new PinResponse("Pin validation failed", pinDto), HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOGGER.error("Exception while validating pin for msisdn:{}", pinDto.getMsisdn(), e);
			response = new ResponseEntity<>(new PinResponse("Unexpected Error, Please contact admin", pinDto), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

}
