package com.docomo.pinmanager.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

@Entity
public class MSISDNEntity {

	@Id
	@Column(name = "msis_dn")
	private String msisdn;
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
	            org.hibernate.annotations.CascadeType.DELETE,
	            org.hibernate.annotations.CascadeType.MERGE,
	            org.hibernate.annotations.CascadeType.PERSIST})
	private List<PINStatus> pinStatus = new ArrayList<>();
	

	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public List<PINStatus> getPinStatus() {
		return pinStatus;
	}

	public void setPinStatus(List<PINStatus> pinStatus) {
		this.pinStatus = pinStatus;
	}

}
