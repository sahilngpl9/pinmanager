package com.docomo.pinmanager.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PINStatus {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private char[] pin;
	private String status;
	private LocalDateTime createTime;
	
	public PINStatus(char[] pin, String status, LocalDateTime createTime) {
		this.pin = pin;
		this.status = status;
		this.createTime = createTime;
	}
	public PINStatus() {
		
	}
	public char[] getPin() {
		return pin;
	}
	public void setPin(char[] pin) {
		this.pin = pin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDateTime getCreateTime() {
		return createTime;
	}
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
