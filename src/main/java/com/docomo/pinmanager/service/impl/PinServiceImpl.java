package com.docomo.pinmanager.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docomo.pinmanager.entity.MSISDNEntity;
import com.docomo.pinmanager.entity.PINStatus;
import com.docomo.pinmanager.exception.PinManagerException;
import com.docomo.pinmanager.model.PinDto;
import com.docomo.pinmanager.repository.MSISDNRepository;
import com.docomo.pinmanager.service.PinService;
import com.docomo.pinmanager.service.PinValidatorService;

@Service
public class PinServiceImpl implements PinService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PinServiceImpl.class);

	private static final String NUMBERS = "0123456789";

	private static final String VALIDATED = "Validated";
	private static final int LENGTH = 4;

	private static final String REJECTED = "Rejected";

	@Autowired
	private MSISDNRepository pinRepository;

	@Autowired
	private PinValidatorService pinValidatorService;

	Random random = new Random();

	@Override
	public PinDto generatePin(PinDto pinDto) throws PinManagerException {
		char[] pin = null;
		String msisdn = pinDto.getMsisdn();
		LOGGER.info("PIN generation started for msisdn:{}", msisdn);
		if (!pinValidatorService.validate(msisdn)) {
			LOGGER.error("MSISDN :{} validation failed", msisdn);
			throw new PinManagerException("Please enter a valid MSISDN which must be of 11 digit and registered with us.");
		}
		if (!pinValidatorService.validateMaxAttempt(msisdn)) {
			LOGGER.error("PIN generation attempted 3 times for msisdn:{}", msisdn);
			throw new PinManagerException("You already attempted 3 times");
		}
		pin = new char[LENGTH];

		for (int i = 0; i < LENGTH; i++) {
			pin[i] = NUMBERS.charAt(random.nextInt(NUMBERS.length()));
		}
		MSISDNEntity entity = pinRepository.findByMsisdn(msisdn);
		if (pin.length == 4) {
			entity.setMsisdn(msisdn);
			List<PINStatus> list = entity.getPinStatus();
			if (!Optional.ofNullable(list).isPresent()) {
				list = new ArrayList<>();
			}
			PINStatus status = new PINStatus(pin, "Generated", LocalDateTime.now());
			if (list.size() < 3) {
				list.add(status);
				entity.setPinStatus(list);
				pinRepository.save(entity);
				pinDto.setPin(String.valueOf(pin));
			}
		}
		return pinDto;
	}

	@Override
	public boolean validatePin(PinDto pinDto) {
		if (!Optional.ofNullable(pinDto.getMsisdn()).isPresent() || !Optional.ofNullable(pinDto.getPin()).isPresent()) {
			LOGGER.error("Not a valid request");
			return false;
		}

		LOGGER.info("Validation started for msisdn:{}", pinDto.getMsisdn());
		MSISDNEntity entity = pinRepository.findByMsisdn(pinDto.getMsisdn());
		List<PINStatus> pinList = entity.getPinStatus();
		String state = "";
		if (Optional.ofNullable(pinList).isPresent()) {
			for (PINStatus status : pinList) {
				if (pinDto.getPin().equals(String.valueOf(status.getPin())) && !status.getStatus().equals(VALIDATED)
						&& !status.getStatus().equals(REJECTED)) {
					LOGGER.info("Validation of Pin successful for msisdn:{}", pinDto.getMsisdn());
					status.setStatus(VALIDATED);
					state = VALIDATED;
					break;
				}
			}
			if (state.equals(VALIDATED)) {
				for (PINStatus status : pinList) {
					if (!status.getStatus().equals(VALIDATED)) {
						status.setStatus(REJECTED);
					}
				}
				pinRepository.save(entity);
				return true;
			}
		}
		return false;
	}
}
