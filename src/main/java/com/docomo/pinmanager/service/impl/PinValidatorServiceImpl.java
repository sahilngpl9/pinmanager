package com.docomo.pinmanager.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docomo.pinmanager.entity.MSISDNEntity;
import com.docomo.pinmanager.entity.PINStatus;
import com.docomo.pinmanager.repository.MSISDNRepository;
import com.docomo.pinmanager.service.PinValidatorService;

@Service
public class PinValidatorServiceImpl implements PinValidatorService {

	@Autowired
	private MSISDNRepository msisdnRepository;

	@Override
	public boolean validate(String msisdn, char[] pin) {
		MSISDNEntity entity = msisdnRepository.findByMsisdn(msisdn);
		List<PINStatus> list = entity.getPinStatus();
		if (Optional.ofNullable(list).isPresent() && !list.isEmpty()) {
			for (PINStatus status : list) {
				if (String.valueOf(status.getPin()).equals(String.valueOf(pin))) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean validate(String msisdn) {
		return (msisdn != null && msisdn.matches("\\d{11}") && msisdnRepository.findByMsisdn(msisdn) != null) ? true
				: false;
	}

	@Override
	public boolean validateMaxAttempt(String msisdn) {
		MSISDNEntity entity = msisdnRepository.findByMsisdn(msisdn);
		List<PINStatus> list = entity.getPinStatus();
		if (Optional.ofNullable(list).isPresent() && list.size() >= 3) {
			return false;
		}
		return true;
	}

}
