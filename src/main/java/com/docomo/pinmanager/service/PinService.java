package com.docomo.pinmanager.service;

import com.docomo.pinmanager.exception.PinManagerException;
import com.docomo.pinmanager.model.PinDto;

public interface PinService {

	public PinDto generatePin(PinDto pinDto) throws PinManagerException;

	public boolean validatePin(PinDto pinDto);
}
