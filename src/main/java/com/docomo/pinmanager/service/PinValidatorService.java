package com.docomo.pinmanager.service;

public interface PinValidatorService {

	public boolean validate(String msisdn, char[] pin);

	public boolean validate(String msisdn);

	public boolean validateMaxAttempt(String msisdn);
}
