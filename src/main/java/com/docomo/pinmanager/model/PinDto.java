package com.docomo.pinmanager.model;

public class PinDto {
	private String msisdn;
	private String pin;

	public PinDto() {
		super();
	}

	public PinDto(String msisdn, String pin) {
		super();
		this.msisdn = msisdn;
		this.pin = pin;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

}
