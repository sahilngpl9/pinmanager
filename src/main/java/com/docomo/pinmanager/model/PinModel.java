package com.docomo.pinmanager.model;

public class PinModel {
 private String msisdn;
 private String pin;
public String getMsisdn() {
	return msisdn;
}
public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}
public String getPin() {
	return pin;
}
public void setPin(String pin) {
	this.pin = pin;
}
 
}
