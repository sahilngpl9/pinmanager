package com.docomo.pinmanager.model;

public class PinResponse {
	private String message;
	private PinDto data;

	public PinResponse(String message, PinDto data) {
		super();
		this.message = message;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PinDto getData() {
		return data;
	}

	public void setData(PinDto data) {
		this.data = data;
	}

}
