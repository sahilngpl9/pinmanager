package com.docomo.pinmanager.schedular;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.docomo.pinmanager.entity.MSISDNEntity;
import com.docomo.pinmanager.entity.PINStatus;
import com.docomo.pinmanager.repository.MSISDNRepository;
import com.docomo.pinmanager.repository.PinStatusRepository;

@Component
@Transactional
public class PinCleanerSchedular {

	private static final Logger LOGGER = LoggerFactory.getLogger(PinCleanerSchedular.class);
	@Autowired
	private PinStatusRepository pinStatusRepository;
	@Autowired
	private MSISDNRepository msisdnRepository;

	@Scheduled(fixedDelayString = "${pinmanager.pin.wipe.interval}")
	public void cleanUpProcess() {
		try {
			LOGGER.info("Pin wipe out process started");
			LocalDateTime expiryTime = LocalDateTime.now().minusMinutes(1);
			List<MSISDNEntity> msisdnEntities = msisdnRepository.findAll();
			List<PINStatus> pinsToBeWiped = new ArrayList<>();
			for (MSISDNEntity entity : msisdnEntities) {
				List<PINStatus> pinStatus = entity.getPinStatus();
				List<PINStatus> activePins = pinStatus.stream().filter(status -> status.getCreateTime().isAfter(expiryTime)
						|| (status.getCreateTime().isBefore(expiryTime) && !status.getStatus().equals("Generated")))
						.collect(Collectors.toList());
				List<PINStatus> pinsToRemoved = pinStatus.stream().filter(
						status -> status.getCreateTime().isBefore(expiryTime) && status.getStatus().equals("Generated"))
						.collect(Collectors.toList());
				pinsToBeWiped.addAll(pinsToRemoved);
				entity.setPinStatus(activePins);
			}
			if (!pinsToBeWiped.isEmpty()) {
				msisdnRepository.saveAll(msisdnEntities);
				pinStatusRepository.deleteAll(pinsToBeWiped);
			}
			LOGGER.info("Pin wipe out process ended");
		} catch (Exception e) {
			LOGGER.error("Exception while wiping pin data", e);
		}
	}
}
