package com.docomo.pinmanager.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.docomo.pinmanager.entity.PINStatus;

@Repository
public interface PinStatusRepository extends JpaRepository<PINStatus, String>{
	public void deleteByCreateTimeBefore(LocalDateTime expiryTime);
}
