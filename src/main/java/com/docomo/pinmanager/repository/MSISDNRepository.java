package com.docomo.pinmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.docomo.pinmanager.entity.MSISDNEntity;

@Repository
public interface MSISDNRepository extends JpaRepository<MSISDNEntity, String> {

	public MSISDNEntity findByMsisdn(String msisdn);
}
