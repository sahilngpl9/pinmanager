package com.docomo.pinmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PinManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PinManagerApplication.class, args);
	}

}
